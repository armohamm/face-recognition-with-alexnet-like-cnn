import cv2
import keras
from keras.models import load_model
from keras.preprocessing import image
import numpy as np
import matplotlib.pyplot as plt
people = ["Adam Sandler", "Alyssa Milano", "Bruce Willis", "Denise Richards", "George Clooney",
    "Gwyneth Paltrow", "Hugh Jackman", "Jason Statham", "Jennifer Love Hewitt" ,"Lindsay Lohan",
    "Mark Ruffalo","Robert Downey Jr","Will Smith"]

imgWidthHeight =  227


def load_image(img_path, show=False, loadFromFile = False):
    if loadFromFile:
        img = image.load_img(img_path, target_size=(imgWidthHeight, imgWidthHeight))
    else:
        img = img_path
    img_tensor = image.img_to_array(img)                    # (height, width, channels)
    img_tensor = np.expand_dims(img_tensor, axis=0)         # (1, height, width, channels), add a dimension because the model expects this shape: (batch_size, height, width, channels)
    #img_tensor /= 255.                                      # imshow expects values in the range [0, 1]

    if show:
        plt.imshow(img_tensor[0])
        plt.axis('off')
        plt.show()

    return img_tensor




modelKeras = keras.models.load_model('alexnetScratch.h5') # load alexnet network


profileCascade = cv2.CascadeClassifier('Cascades/haarcascade_frontalface_default.xml') #load modelKeras

image_path = 'test.jpg' # Test image input


im = cv2.imread(image_path)
gray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
cv2.imshow('Image in input', im)
cv2.waitKey(0);



faces = profileCascade.detectMultiScale(
    gray,
    scaleFactor=1.1,
    minNeighbors=4,
)

# loop through detected faces and add bounding box


for (x,y,w,h) in faces:
    # draw rectangle over face
    cv2.rectangle(im, (x,y),(x+w,y+h), (255,0,0), 2)

    roi_color = im[y:y+h, x:x+w]
    finalFace = cv2.resize(roi_color, (imgWidthHeight,imgWidthHeight))
    cv2.imshow('Faces', finalFace)
    cv2.waitKey(0); cv2.destroyAllWindows()

    finalFace = load_image(finalFace) # We convert the image into a tensor

    # Prediction!
    pred = modelKeras.predict(finalFace, batch_size = 32)
    #pred = np.array(pred)
    y_classes = pred.argmax(axis=-1)
    print("IMAGE")
    print(people[int(y_classes)] + " with accuracy " + str(np.max(pred)*100) + "%")

    # IMPLEMENTED ONLY FOR OBTAINING INTERMEDIATE STEPS FOR REPORT
    #layer_outputs = [layer.output for layer in modelKeras.layers[:13]] # Extracts the outputs of the top 13 layers
    #activation_model = keras.models.Model(inputs=modelKeras.input, outputs=layer_outputs) # Creates a model that will return these outputs, given the model input


    #layer_names = []
    #for layer in modelKeras.layers[:12]:
    #    layer_names.append(layer.name) # Names of the layers, so you can have them as part of your plot

    #images_per_row = 16
    #activations = activation_model.predict(finalFace)
    #for layer_name, layer_activation in zip(layer_names, activations): # Displays the feature maps
    #    n_features = layer_activation.shape[-1] # Number of features in the feature map
    #    size = layer_activation.shape[1] #The feature map has shape (1, size, size, n_features).
    #    n_cols = n_features // images_per_row # Tiles the activation channels in this matrix
    #    display_grid = np.zeros((size * n_cols, images_per_row * size))
    #    for col in range(n_cols): # Tiles each filter into a big horizontal grid
    #        for row in range(images_per_row):
    #            channel_image = layer_activation[0,
    #                                            :, :,
    #                                            col * images_per_row + row]
    #            channel_image -= channel_image.mean() # Post-processes the feature to make it visually palatable
    #            channel_image /= channel_image.std()
    #            channel_image *= 64
    #            channel_image += 128
    #            channel_image = np.clip(channel_image, 0, 255).astype('uint8')
    #            display_grid[col * size : (col + 1) * size, # Displays the grid
    #                        row * size : (row + 1) * size] = channel_image
    #    scale = 1. / size
    #    plt.figure(figsize=(scale * display_grid.shape[1],
    #                    scale * display_grid.shape[0]))
    #    plt.title(layer_name)
    #    plt.grid(False)
    #    plt.imshow(display_grid, aspect='auto', cmap='viridis')
    #    plt.show()




input_movie = cv2.VideoCapture('bruce.mp4')

#while it in range(length):
while input_movie.isOpened():
    # Capture frame-by-frame
    ret, frame = input_movie.read()


    profileFace = profileCascade.detectMultiScale(frame,1.2,5)

             #Draw a rectangle around the faces
    for (x, y, w, h) in profileFace:
        cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 150, 150), 1)
        roi_color = frame[y:y+h, x:x+w]
        finalFace = cv2.resize(roi_color, (imgWidthHeight,imgWidthHeight))
        #cv2.imshow('Faces', finalFace)
        #cv2.waitKey(1);
        finalFace = load_image(finalFace)
        # Prediction!
        pred = modelKeras.predict(finalFace,batch_size = 32)
        pred = np.array(pred)
        y_classes = pred.argmax(axis=-1)
        #print(pred)
        #print(y_classes)
        font = cv2.FONT_HERSHEY_SIMPLEX
        text = people[int(y_classes)] + " - Acc:" + str(int(np.max(pred)*100)) + "%"
        cv2.putText(frame,text,(x,y), font, 0.5, (0,150,150))


    cv2.imshow('video',frame)

    if cv2.waitKey(25) & 0xFF == ord('q'):
        break

input_movie.release()
cv2.destroyAllWindows()
